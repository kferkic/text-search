package de.hska.simo.textSearch;

import java.io.Reader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import javafx.beans.binding.StringBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import de.hska.simo.textSearch.model.TextSearch;
import de.hska.simo.textSearch.model.callback.Debugger;
import de.hska.simo.textSearch.model.callback.MatchEmitter;
import de.hska.simo.textSearch.model.callback.ProgressCallback;
import de.hska.simo.textSearch.model.fsm.FSMStartState;
import de.hska.simo.textSearch.model.fsm.FSMState;

public class DebugController implements Debugger {

	@FXML
	private Pane container;

	@FXML
	private Button nextButton;

	@FXML
	private TableView<FSMState> infoTable;

	@FXML
	private TableColumn<FSMState, String> stateColumn;

	@FXML
	private TableColumn<FSMState, String> failureColumn;

	@FXML
	private TableColumn<FSMState, String> outputColumn;

	@FXML
	private ListView<String> logList;

	private ObservableList<String> logListModel;

	private TextSearch textSearch;

	private Map<FSMState, StateView> stateViews = new HashMap<FSMState, StateView>();

	private Stage stage;

	private ProgressCallback progress;

	private StateView currentSelected;

	private Map<FSMState, String> stateIndices = new HashMap<>();

	@FXML
	private void initialize() {
		logListModel = FXCollections.observableArrayList();
		logList.setItems(logListModel);
	}

	@FXML
	private void next() {
		textSearch.next();
	}

	private void initStateIndices(FSMState startState) {
		int counter = 0;
		Queue<FSMState> states = new LinkedList<>();
		states.add(startState);
		while (!states.isEmpty()) {
			FSMState state = states.poll();
			stateIndices.put(state, String.valueOf(counter++));
			for (FSMState successorState : state.getTransitions().values()) {
				states.add(successorState);
			}
		}
	}

	private void addStateViews(FSMState state) {
		addStateViews(state, 0, 0);
	}

	private int addStateViews(FSMState state, int x, int y) {
		boolean isFinite = state.getOutput() != null
				&& !state.getOutput().isEmpty();
		String failState = null;
		if (state.getFailState() != null) {
			failState = stateIndices.get(state.getFailState());
		}
		String stateLabel = stateIndices.get(state);
		StateView stateView = new StateView(x, y, isFinite, stateLabel,
				failState);

		stateViews.put(state, stateView);
		Map<Character, FSMState> transitions = state.getTransitions();
		for (Iterator<Character> transitionItr = transitions.keySet()
				.iterator(); transitionItr.hasNext();) {
			Character transition = transitionItr.next();
			stateView.addTransition(x + 1, y, transition);
			if (state instanceof FSMStartState) {
				stateView.addSelfTransition(transitions.keySet());
			}
			y = addStateViews(transitions.get(transition), x + 1, y);
			if (transitionItr.hasNext()) {
				++y;
			}
		}

		return y;
	}

	private class StateView extends Pane {
		private Circle c;
		private Map<Character, CubicCurve> transitions = new HashMap<Character, CubicCurve>();

		private int offset = 100;
		private int radius = 20;

		private int row;
		private int column;
		private String label;

		public StateView(int column, int row, boolean isFinite, String label,
				String failState) {
			this.row = row;
			this.column = column;
			this.label = label;

			c = new Circle(offset * (column + 1), offset * (row + 1), radius);
			c.setFill(Color.TRANSPARENT);
			c.setStroke(Color.BLACK);

			getChildren().add(c);
			if (isFinite) {
				Circle inner = new Circle(offset * (column + 1), offset
						* (row + 1), 15);
				inner.setFill(Color.TRANSPARENT);
				inner.setStroke(Color.BLACK);
				getChildren().add(inner);
			}
			Text t = new Text(label);
			double textWidth = t.getLayoutBounds().getWidth();
			t.setTextAlignment(TextAlignment.CENTER);
			t.setTextOrigin(VPos.CENTER);
			t.setLayoutX(offset * (column + 1) - textWidth / 2);
			t.setLayoutY(offset * (row + 1));
			getChildren().add(t);

			if (failState != null) {
				Text fail = new Text("f = " + failState);
				fail.setFont(Font.font(8));
				double failTextWidth = fail.getLayoutBounds().getWidth();
				fail.setTextAlignment(TextAlignment.CENTER);
				fail.setTextOrigin(VPos.CENTER);
				fail.setLayoutX(offset * (column + 1) - failTextWidth / 2);
				fail.setLayoutY(offset * (row + 1) - 30);
				fail.setFill(Color.RED);
				getChildren().add(fail);
			}
		}

		public void select() {
			c.setStrokeWidth(3);
		}

		public void addSelfTransition(Collection<Character> except) {
			double centerX = c.getCenterX() - c.getRadius();
			double centerY = c.getCenterY() - c.getRadius();
			double radius = c.getRadius();
			Arc transition = new Arc(centerX, centerY, radius, radius, 0, 270);
			transition.setFill(Color.TRANSPARENT);
			transition.setStroke(Color.BLACK);
			getChildren().add(transition);

			String exceptString = except.toString();
			exceptString = exceptString.replaceAll("^\\[(.*)\\]$", "{$1}");
			exceptString = "\u00AC " + exceptString;
			Text t = new Text(exceptString);
			t.setTextAlignment(TextAlignment.CENTER);
			t.setTextOrigin(VPos.CENTER);
			t.setFont(Font.font(10));

			double textWidth = t.getLayoutBounds().getWidth();
			t.setLayoutX(centerX - textWidth / 2);
			t.setLayoutY(centerY - radius - 10);
			getChildren().add(t);
		}

		public void addTransition(int x, int y, Character transition) {
			int startX;
			int startY;
			int endX;
			int endY;
			if (row < y) {
				startX = offset * (column + 1);
				startY = offset * (row + 1) + radius;
			} else {
				startX = offset * (column + 1) + radius;
				startY = offset * (row + 1);
			}
			endX = offset * (x + 1) - radius;
			endY = offset * (y + 1);

			CubicCurve curve = new CubicCurve(startX, startY, startX, endY,
					startX, endY, endX, endY);
			curve.setFill(Color.TRANSPARENT);
			curve.setStroke(Color.BLACK);
			transitions.put(transition, curve);
			getChildren().add(curve);

			if (row < y) {
				getChildren().add(
						new Text(endX - 15, endY - 5, transition.toString()));
			} else {
				getChildren().add(
						new Text(startX + (endX - startX) / 2, startY
								+ (endY - startY) / 2 - 5, transition
								.toString()));
			}
		}

		public void deselect() {
			c.setStrokeWidth(1);
		}
	}

	@Override
	public void transition(char c, FSMState fromState, FSMState toState) {
		c = c == ' ' ? '\u2423' : c;
		c = c == '\n' ? '\u240A' : c;
		c = c == '\r' ? '\u240D' : c;
		currentSelected.deselect();
		currentSelected = stateViews.get(toState);
		currentSelected.select();
		String log = "g (" + stateIndices.get(fromState) + ", '" + c + "') -> "
				+ stateIndices.get(toState);
		String logIndex = String.valueOf(logListModel.size() + 1);
		logListModel.add(0, logIndex + ":     " + log);
	}

	@Override
	public void fail(char c, FSMState fromState, FSMState toState) {
		c = c == ' ' ? '\u2423' : c;
		c = c == '\n' ? '\u240A' : c;
		c = c == '\r' ? '\u240D' : c;
		currentSelected.deselect();
		currentSelected = stateViews.get(toState);
		currentSelected.select();
		String log = "g (" + stateIndices.get(fromState) + ", '" + c + "') -> "
				+ "f (" + stateIndices.get(fromState) + ") -> "
				+ stateIndices.get(fromState.getFailState());
		String logIndex = String.valueOf(logListModel.size() + 1);
		logListModel.add(0, logIndex + ":     " + log);
	}

	public void initDebugger(MatchEmitter emitter, ProgressCallback progress,
			Reader text, String[] searchKeywords) {
		this.progress = progress;
		textSearch = TextSearch.debug(this, emitter, progress, text,
				searchKeywords);
		FSMState startState = textSearch.getStartState();
		if (startState != null) {
			initStateIndices(startState);
			addStateViews(startState);

			for (StateView stateView : stateViews.values()) {
				container.getChildren().add(stateView);
			}

			for (FSMState state : stateViews.keySet()) {
				StateView stateView = stateViews.get(state);
				Rectangle tooltipBox = new Rectangle();
				tooltipBox.setFill(Color.TRANSPARENT);
				tooltipBox.setX(stateView.c.getCenterX()
						- stateView.c.getRadius());
				tooltipBox.setY(stateView.c.getCenterY()
						- stateView.c.getRadius());
				tooltipBox.setWidth(stateView.c.getRadius() * 2);
				tooltipBox.setHeight(stateView.c.getRadius() * 2);
				container.getChildren().add(tooltipBox);
				Tooltip.install(tooltipBox, initTooltipForState(state));
			}

			initInfoTable(startState);

			stateViews.get(startState).select();
			currentSelected = stateViews.get(startState);
		}
	}

	private Tooltip initTooltipForState(FSMState state) {
		StateView stateView = stateViews.get(state);
		String tooltipText = "state: " + stateView.label;
		tooltipText += "\n";
		String failState = stateIndices.get(state.getFailState());
		tooltipText += "f (" + stateView.label + "): "
				+ (failState == null ? "-" : failState);
		tooltipText += "\n";
		tooltipText += "o (" + stateView.label + "): ";
		String output = state.getOutput() != null
				&& !state.getOutput().isEmpty() ? state.getOutput().toString()
				.replaceAll("^\\[(.*)\\]$", "{$1}") : "\u2205";
		tooltipText += output;

		Tooltip tooltip = new Tooltip(tooltipText);
		return tooltip;
	}

	private void initInfoTable(FSMState startState) {
		stateColumn.setCellValueFactory((param) -> {
			return new StringBinding() {
				@Override
				protected String computeValue() {
					return stateIndices.get(param.getValue());
				}
			};
		});
		stateColumn.setComparator((a, b) -> {
			int aInt = Integer.parseInt(a);
			int bInt = Integer.parseInt(b);
			return aInt - bInt;
		});

		failureColumn.setCellValueFactory((param) -> {
			return new StringBinding() {
				@Override
				protected String computeValue() {
					String failState = stateIndices.get(param.getValue()
							.getFailState());
					failState = failState == null ? "-" : failState;
					return failState;
				}
			};
		});
		failureColumn.setComparator((a, b) -> {
			a = "-".equals(a) ? "-1" : a;
			b = "-".equals(b) ? "-1" : b;
			int aInt = Integer.parseInt(a);
			int bInt = Integer.parseInt(b);
			return aInt - bInt;
		});

		outputColumn.setCellValueFactory((param) -> {
			return new StringBinding() {
				@Override
				protected String computeValue() {
					String outputString = null;
					Collection<String> output = param.getValue().getOutput();
					if (output == null || output.isEmpty()) {
						outputString = "\u2205";
					} else {
						outputString = output.toString().replaceAll(
								"^\\[(.*)\\]$", "{$1}");
					}
					return outputString;
				}
			};
		});

		ObservableList<FSMState> states = FXCollections.observableArrayList();
		Queue<FSMState> q = new LinkedList<>();
		q.add(startState);
		while (!q.isEmpty()) {
			FSMState state = q.poll();
			states.add(state);
			for (FSMState childState : state.getTransitions().values()) {
				q.add(childState);
			}
		}

		infoTable.setItems(states);
	}

	public void setStage(Stage debugStage) {
		this.stage = debugStage;
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				progress.done();
			}
		});
	}

	@Override
	public void done() {
		nextButton.setDisable(true);
		nextButton.setText("done");
		logListModel.add(0, "DONE");
	}

	@Override
	public void output(String output) {
		String logIndex = String.valueOf(logListModel.size() + 1);
		logListModel.add(0, logIndex + ":     out -> \"" + output + "\"");
	}
}
