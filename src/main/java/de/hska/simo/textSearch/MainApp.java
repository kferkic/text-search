package de.hska.simo.textSearch;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainApp extends Application {

	private VBox rootLayout;

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Text-Search");

		try {
			FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("view/TextSearch.fxml"));
			rootLayout = (VBox) loader.load();
			((TextSearchController) loader.getController()).setStage(primaryStage);
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);

			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
