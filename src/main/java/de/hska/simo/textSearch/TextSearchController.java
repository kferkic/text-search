package de.hska.simo.textSearch;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;

import org.apache.commons.io.FileUtils;

import de.hska.simo.textSearch.model.TextSearch;
import de.hska.simo.textSearch.model.callback.MatchEmitter;
import de.hska.simo.textSearch.model.callback.ProgressCallback;
import de.hska.simo.textSearch.model.data.Match;
import de.hska.simo.textSearch.view.MatchCell;

public class TextSearchController implements MatchEmitter, ProgressCallback {

	private static final String KEYWORD_SEPARATOR = " ";

	private Stage primaryStage;

	@FXML
	private TextField searchField;

	@FXML
	private TextArea searchTextArea;

	@FXML
	private Button searchButton;

	@FXML
	private Button debugButton;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private ListView<Match> matchesList;

	@FXML
	private MenuItem openMenuItem;

	private ObservableList<Match> matches = FXCollections.observableArrayList();

	public void setStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	@FXML
	private void initialize() {
		matchesList.setItems(matches);
		matchesList
				.setCellFactory(new Callback<ListView<Match>, ListCell<Match>>() {
					@Override
					public ListCell<Match> call(ListView<Match> list) {
						return new MatchCell();
					}
				});
		matchesList.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Match>() {
					@Override
					public void changed(
							ObservableValue<? extends Match> observable,
							Match oldValue, Match newValue) {
						if (newValue != null) {
							searchTextArea.selectRange(newValue.getCharIndex(),
									newValue.getCharIndex()
											+ newValue.getMatch().length());
						} else {
							searchTextArea.deselect();
						}
					}
				});
	}

	@FXML
	private void search() {
		matches.clear();
		String text = getSearchKeywords();
		String[] keywords = text.split(KEYWORD_SEPARATOR);
		StringReader searchTextReader = new StringReader(
				searchTextArea.getText());
		TextSearch.search(this, this, searchTextReader, keywords);
	}

	@FXML
	private void searchKeywordsChanged() {
		String searchText = getSearchKeywords();
		if (!searchText.isEmpty()) {
			searchButton.setDisable(false);
			debugButton.setDisable(false);
		} else {
			searchButton.setDisable(true);
			debugButton.setDisable(true);
		}
	}

	@FXML
	private void open() {
		FileChooser fc = new FileChooser();
		fc.setTitle("Open text file");
		fc.getExtensionFilters().add(new ExtensionFilter("Text file", "*.txt"));
		File file = fc.showOpenDialog(primaryStage);
		try {
			if (file != null) {
				String fileContent = FileUtils.readFileToString(file);
				searchTextArea.setText(fileContent);
			}
		} catch (IOException e) {
			// the file couldn't be opened...
			// i don't know... i'm too lazy for some serious error-handling
			e.printStackTrace();
		}

	}

	@FXML
	private void quit() {
		System.exit(0);
	}

	@FXML
	private void debug() {
		Stage debugStage = new Stage();
		debugStage.setTitle("Text-Search - DEBUG");

		try {
			FXMLLoader loader = new FXMLLoader(
					MainApp.class.getResource("view/Debug.fxml"));
			SplitPane debugLayout = (SplitPane) loader.load();
			String text = getSearchKeywords();
			String[] keywords = text.split(KEYWORD_SEPARATOR);
			((DebugController) loader.getController()).setStage(debugStage);
			((DebugController) loader.getController()).initDebugger(this, this,
					new StringReader(searchTextArea.getText()), keywords);
			Scene scene = new Scene(debugLayout);
			debugStage.setScene(scene);

			debugStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getSearchKeywords() {
		String text = searchField.getText().trim();
		return text;
	}

	@Override
	public void match(int charIndex, int line, int column, String keyword) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Match match = new Match(keyword, charIndex, line, column);
				String text = searchTextArea.getText();

				StringBuilder prefixBuilder = new StringBuilder();
				for (int i = charIndex - 1; i >= charIndex - 16; --i) {
					if (i >= 0) {
						char character = text.charAt(i);
						if (character != '\n' && character != '\r') {
							if (i <= charIndex - 16) {
								prefixBuilder.insert(0, "...");
								break;
							} else {
								prefixBuilder.insert(0, character);
							}
						} else {
							break;
						}
					} else {
						break;
					}
				}
				StringBuilder postBuilder = new StringBuilder();
				for (int i = charIndex + keyword.length(); i < charIndex
						+ keyword.length() + 16; ++i) {
					if (i < text.length()) {
						char character = text.charAt(i);
						if (character != '\n' && character != '\r') {
							if (i >= charIndex + keyword.length() + 15) {
								postBuilder.append("...");
							} else {
								postBuilder.append(character);
							}
						} else {
							break;
						}
					} else {
						break;
					}
				}

				match.setPrefix(prefixBuilder.toString());
				match.setPostfix(postBuilder.toString());
				matches.add(match);
			}
		});
	}

	@Override
	public void updateProgress(final int processed) {
		int characterCount = searchTextArea.getText().length();
		double progress = processed / (double) characterCount;
		if (progress - progressBar.getProgress() > 0.01) {
			Platform.runLater(() -> {
				progressBar.setProgress(progress);
			});
		}
	}

	@Override
	public void start() {
		updateProgress(0);
		Platform.runLater(() -> {
			matches.clear();
			searchTextArea.setDisable(true);
			searchField.setDisable(true);
			searchButton.setDisable(true);
			debugButton.setDisable(true);
			openMenuItem.setDisable(true);
		});
	}

	@Override
	public void done() {
		Platform.runLater(() -> {
			progressBar.setProgress(0);
			searchTextArea.setDisable(false);
			searchField.setDisable(false);
			searchButton.setDisable(false);
			debugButton.setDisable(false);
			openMenuItem.setDisable(false);
		});
	}
}
