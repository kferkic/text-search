package de.hska.simo.textSearch.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

import de.hska.simo.textSearch.model.callback.Debugger;
import de.hska.simo.textSearch.model.callback.MatchEmitter;
import de.hska.simo.textSearch.model.callback.ProgressCallback;
import de.hska.simo.textSearch.model.fsm.FSMStartState;
import de.hska.simo.textSearch.model.fsm.FSMState;
import de.hska.simo.textSearch.model.fsm.FiniteStateMachine;

public class TextSearch extends FiniteStateMachine implements Runnable {

	private MatchEmitter matchEmitter;
	private ProgressCallback progress;
	private Reader text;
	private Debugger debugger;

	private int line = 1;
	private int column = 1;
	private int charIndex = 0;

	private TextSearch(MatchEmitter matchEmitter, ProgressCallback progress,
			Reader text, String[] keywords) {
		super(keywords, matchEmitter);
		this.matchEmitter = matchEmitter;
		this.progress = progress;
		this.text = new BufferedReader(text);
	}

	public static void search(MatchEmitter emitter, ProgressCallback progress,
			Reader text, String... searchKeywords) {
		new Thread(new TextSearch(emitter, progress, text, searchKeywords))
				.start();
	}

	public FSMStartState getStartState() {
		return this.startState;
	}

	public static void search(MatchEmitter emitter, Reader text,
			String... searchKeywords) {
		search(emitter, text, searchKeywords);
	}

	@Override
	public void run() {
		try {
			if (progress != null) {
				progress.start();
			}
			line = 1;
			column = 1;
			charIndex = 0;
			for (int c = text.read(); c != -1; c = text.read()) {
				next(c);
			}
			if (progress != null) {
				progress.done();
			}
		} catch (IOException e) {
			// do'h
			e.printStackTrace();
		}
	}

	private void next(int c) {
		++charIndex;
		process((char) c);
		if (c != '\r' && c != '\n') {
			++column;
		} else if (c == '\n') {
			column = 1;
			++line;
		}
		if (progress != null) {
			progress.updateProgress(charIndex);
		}
	}

	public void next() {
		int c;
		try {
			c = text.read();
			if (c != -1) {
				next(c);
			} else {
				if (debugger != null) {
					debugger.done();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void output(String output) {
		matchEmitter.match(charIndex - output.length(), line,
				column - output.length(), output);
		if (debugger != null) {
			debugger.output(output);
		}
	}

	public static TextSearch debug(Debugger debugger, MatchEmitter emitter,
			ProgressCallback progress, Reader text, String[] searchKeywords) {
		TextSearch debugTS = new TextSearch(emitter, progress, text,
				searchKeywords);
		debugTS.debugger = debugger;
		if (progress != null) {
			progress.start();
		}
		debugTS.line = 1;
		debugTS.column = 1;
		debugTS.charIndex = 0;
		return debugTS;
	}

	@Override
	public void debugFailState(char c, FSMState fromState, FSMState toState) {
		if (debugger != null) {
			debugger.fail(c, fromState, toState);
		}
	}

	@Override
	public void debugTransition(char c, FSMState fromState, FSMState toState) {
		if (debugger != null) {
			debugger.transition(c, fromState, toState);
		}
	}

}
