package de.hska.simo.textSearch.model.callback;

import de.hska.simo.textSearch.model.fsm.FSMState;

public interface Debugger {

	void transition(char c, FSMState fromState, FSMState toState);

	void fail(char c, FSMState fromState, FSMState toState);

	void done();

	void output(String output);

}
