package de.hska.simo.textSearch.model.callback;

public interface MatchEmitter {

	void match(int totalCharCount, int line, int column, String keyword);

}
