package de.hska.simo.textSearch.model.callback;

public interface ProgressCallback {

	void start();

	void updateProgress(int processed);

	void done();

}
