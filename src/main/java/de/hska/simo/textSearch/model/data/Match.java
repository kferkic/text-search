package de.hska.simo.textSearch.model.data;

public class Match {

	private String match;
	private String prefix;
	private String postfix;
	private int charIndex;
	private int line;
	private int column;

	public Match(String match, int charIndex, int line, int column) {
		super();
		this.match = match;
		this.charIndex = charIndex;
		this.line = line;
		this.column = column;
	}

	public String getMatch() {
		return match;
	}

	public void setMatch(String match) {
		this.match = match;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public int getCharIndex() {
		return charIndex;
	}

	public void setCharIndex(int charIndex) {
		this.charIndex = charIndex;
	}

}
