package de.hska.simo.textSearch.model.fsm;

public class FSMStartState extends FSMState {

	public FSMStartState(FiniteStateMachine stateMachine) {
		super(stateMachine);
	}

	public FSMState getStateForTransition(char c) {
		FSMState state = transitions.get(Character.valueOf(c));
		if (state == null) {
			stateMachine.debugTransition(c, this, this);
			return this;
		} else {
			stateMachine.debugTransition(c, this, state);
			return state;
		}
	}

}
