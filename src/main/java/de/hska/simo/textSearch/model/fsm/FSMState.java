package de.hska.simo.textSearch.model.fsm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FSMState {

	Map<Character, FSMState> transitions;

	private Collection<String> output;

	private FSMState failState;

	protected FiniteStateMachine stateMachine;

	public FSMState(FiniteStateMachine stateMachine) {
		transitions = new HashMap<Character, FSMState>();
		output = new ArrayList<String>();
		this.stateMachine = stateMachine;
	}

	public FSMState getStateForTransition(char c) {
		FSMState state = transitions.get(Character.valueOf(c));
		if (state == null) {
			stateMachine.debugFailState(c, this, failState);
			state = failState.getStateForTransition(c);
		} else {
			stateMachine.debugTransition(c, this, state);
		}
		return state;
	}

	public FSMState createStateForTransition(char c) {
		FSMState state = new FSMState(stateMachine);
		transitions.put(Character.valueOf(c), state);
		return state;
	}

	public void setFailState(FSMState state) {
		this.failState = state;
	}

	public FSMState getFailState() {
		return failState;
	}

	public Collection<String> getOutput() {
		return output;
	}

	public void addOutput(Collection<String> output) {
		this.output.addAll(output);
	}

	public void addOutput(String output) {
		this.output.add(output);
	}

	public void enter() {
		for (String out : output) {
			stateMachine.output(out);
		}
	}

	public Map<Character, FSMState> getTransitions() {
		return transitions;
	}

}
