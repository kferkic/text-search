package de.hska.simo.textSearch.model.fsm;

import java.util.LinkedList;
import java.util.Queue;

import de.hska.simo.textSearch.model.callback.MatchEmitter;

public abstract class FiniteStateMachine {

	protected FSMState currentState;

	protected FSMStartState startState;

	public FiniteStateMachine(String[] keywords, MatchEmitter matchEmitter) {
		startState = new FSMStartState(this);
		currentState = startState;
		createStateMachine(keywords);
	}

	private void createStateMachine(String[] keywords) {
		for (String keyword : keywords) {
			FSMState tmpState = currentState;
			for (int i = 0; i < keyword.length(); ++i) {
				char currentChar = keyword.charAt(i);
				FSMState nextState = tmpState.transitions.get(currentChar);
				if (nextState == null) {
					nextState = tmpState.createStateForTransition(currentChar);
				}
				if (i + 1 == keyword.length()) {
					nextState.addOutput(keyword);
				}
				tmpState = nextState;
			}
		}

		initFailureFunctions();
	}

	private void initFailureFunctions() {
		Queue<FSMState> queue = new LinkedList<FSMState>();
		for (FSMState state : currentState.transitions.values()) {
			queue.add(state);
			state.setFailState(currentState);
		}

		while (!queue.isEmpty()) {
			FSMState r = queue.poll();
			for (Character c : r.transitions.keySet()) {
				FSMState s = r.transitions.get(c);
				queue.add(s);
				FSMState state = r.getFailState();
				// while (!(state instanceof FSMStartState) &&
				// state.getStateForTransition(c) != null) {
				// state = state.getFailState();
				// }
				s.setFailState(state.getStateForTransition(c));
				s.addOutput(s.getFailState().getOutput());
			}

		}

	}

	public void process(char c) {
		currentState = currentState.getStateForTransition(c);
		currentState.enter();
	}

	public void output(String output) {
		// do nothing here
	}

	public abstract void debugFailState(char c, FSMState fromState,
			FSMState toState);

	public abstract void debugTransition(char c, FSMState fromState,
			FSMState toState);

}
