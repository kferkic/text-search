package de.hska.simo.textSearch.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import de.hska.simo.textSearch.model.data.Match;

public class MatchCell extends ListCell<Match> {

	private VBox root;

	private Label matchLabel;

	private Label prefix;
	private Label postfix;

	private Label detailLabel;

	public MatchCell() {
		root = new VBox();
		HBox matchContainer = new HBox();
		matchContainer.setAlignment(Pos.BOTTOM_LEFT);
		root.getChildren().add(matchContainer);

		matchLabel = new Label();
		matchLabel.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, 15));
		prefix = new Label();
		postfix = new Label();
		matchContainer.getChildren().addAll(prefix, matchLabel, postfix);

		detailLabel = new Label();
		detailLabel.setFont(Font.font(10));
		detailLabel.setPadding(new Insets(0, 0, 0, 25));

		root.getChildren().addAll(detailLabel);
	}

	@Override
	protected void updateItem(Match match, boolean empty) {
		super.updateItem(match, empty);
		if (empty) {
			clearContent();
		} else {
			addContent(match);
		}
	}

	private void addContent(Match match) {
		setText(null);
		setGraphic(root);
		matchLabel.setText(match.getMatch());
		prefix.setText(match.getPrefix());
		postfix.setText(match.getPostfix());
		detailLabel.setText("Line: " + match.getLine() + ", Column: " + (match.getColumn() + 1));
	}

	private void clearContent() {
		setText(null);
		setGraphic(null);
	}

}
